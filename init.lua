local rms_is_present = {}

local rms_skybox = {
    "stars.png",
    "stars.png",
    "rms.png",
    "stars.png",
    "stars.png",
    "stars.png",
}

local function summon_rms(player_name)
    rms_is_present[player_name] = minetest.sound_play("rms", {
        to_player = player_name,
        gain = 2.0
    })

    minetest.get_player_by_name(player_name):set_sky({}, "skybox", rms_skybox)
end

local function dismiss_rms(player_name)
    minetest.get_player_by_name(player_name):set_sky({}, "regular", {})

    if rms_is_present[player_name] ~= nil then
        minetest.sound_stop(rms_is_present[player_name])
    end

    rms_is_present[player_name] = nil
end


minetest.register_on_joinplayer(function(player)
        rms_is_present[player:get_player_name()] = nil
    end
)

minetest.register_chatcommand("rms", {
    func = function(name, param)

        -- Summon RMS if he isn't already present
        if rms_is_present[name] == nil then
            minetest.chat_send_player(name, "*You feel the sudden urge to install Gentoo...*")
            summon_rms(name)

            minetest.after(146, function()
                dismiss_rms(name)
            end
            )
        -- Dismiss RMS if he is present
        else
            minetest.chat_send_player(name, "OH NO! Your proprietary software scared RMS away!")
            dismiss_rms(name)
        end
    end
})
